FROM python:2.7

RUN mkdir -p /app & \
    apt-get update && \
    apt-get install -y libmariadbclient-dev mariadb-client && \
    rm -vrf /var/lib/apt/lists

WORKDIR /app

ADD . /app/

RUN pip install -r requirements.txt

EXPOSE 8000

CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
