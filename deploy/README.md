# Deploy with kubernetes

This manual assumes a working kubernetes environment.

## Steps

First, add your password in secrets directory:

```
echo -n "macoy123" > deploy/secrets/DATABASE_PASSWORD
echo -n "123456" > deploy/secrets/EMAIL_HOST_PASSWORD
```

Then, create your secrets:

```
bash -x deploy/create-secrets.sh
```

Then, apply the definitions of services and deployments:

```
bash -x deploy/apply-all.sh
```

Now you may see the app deployed with:

```
kubectl get services,pods,pvc,pv,deployments
```

# If you modify the code

Serives and deployment definitions uses `vsis/quest-to-devops` container image.
If you can't push code to that repository, you need to change image name in three files:

  - `docker-compose.yml`
  - `deploy/quest-to-devops-deployment.yaml`
  - `deploy/celery-deployment.yaml`

To do that, run:

```
sed -i "s#vsis/quest-to-devops#my-image#" deploy/quest-to-devops-deployment.yaml
sed -i "s#vsis/quest-to-devops#my-image#" docker-compose.yml
sed -i "s#vsis/celery#my-celery-image#" deploy/celery-deployment.yaml
sed -i "s#vsis/celery#my-celery-image#" docker-compose.yml
```

Then build and push images

```
docker-compose build
docker push my-image
docker push my-celery-image
```

Now you may deploy a modified version.



