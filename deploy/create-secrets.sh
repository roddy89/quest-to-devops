#!/bin/bash

kubectl create secret generic passwords \
    --from-file deploy/secrets/DATABASE_PASSWORD \
    --from-file deploy/secrets/EMAIL_HOST_PASSWORD
