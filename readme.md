
# Test DevOps

El siguiente repositorio, corresponde a un desafío que te proponemos como OSA para poder ser parte nuestro equipo de desarrollo.

**Instrucciones**

-	Forkear el proyecto

-	Utilizar minikube.

-	Implementar el proyecto en Docker

-	Implementar los correspondientes **secrets** en kubernetes, que se encuentran en el archivo **.env.example**
-	Implementar un servicio de mysql común para el deployment del proyecto, con un **volumen persistente.**
-	Implementar un deployment con:
	-	El proyecto “devops” (python manage.py runserver)
	-	celery worker (celery worker --time-limit=300 --concurrency=8 --app=devops --loglevel=INFO)
	-	redis (se aconseja que este dentro del mismo servicio)
	-	Agregar los secrets como variables de entorno
-	Generar los .yaml de los pods, controladores y servicios  necesarios para correr este ejemplo en minikube.
-	Generar documentación para dejar corriendo el proyecto en minikube.
-	Enviar y dar acceso del bucket a richard@osacontrol.com y gmunoz@osacontrol.com. 
