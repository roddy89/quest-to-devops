# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from django.views.generic import CreateView

from main.forms import ContactForm
from main.models import Contact
from main.tasks import send_contact_mail


class ContactView(CreateView):
    raise_exception = True
    template_name = "contact-form/index.html"
    model = Contact
    form_class = ContactForm

    def form_valid(self, form):
        form_tmp = form.save(commit=False)
        #  send async email
        send_contact_mail.delay({
            'subject': 'Has recibido un contacto de %s' % (form_tmp.name),
            'message': '%s, ha dejado el siguiente mensaje %s. %s' % (
                form_tmp.name,
                form_tmp.message,
                'Puedes responderle al correo %s' % (form_tmp.email)
            ),
            'recipient_list':['roddy.gonzalez.89@gmail.com',]
        })
        return super(ContactView, self).form_valid(form)

    def form_invalid(self, form):
        return self.render_to_response(self.get_context_data(form=form))

    def get_success_url(self):
        return reverse('home')
