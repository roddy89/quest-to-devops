# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible


@python_2_unicode_compatible
class Contact(models.Model):
    name = models.CharField('Nombre', max_length=200, blank=False)
    email = models.EmailField('e-mail', max_length=200, blank=False)
    message = models.TextField('Mensaje', blank=False)
    created = models.DateTimeField(
        'Created',
        auto_now_add=True,
        editable=False,
        help_text='The date and time this register was created.'
    )
    updated = models.DateTimeField(
        'Updated',
        auto_now=True,
        editable=False,
        help_text='The date and time this register was updated.'
    )

    class Meta:
        ordering = ['created', ]

    def __str__(self):
        return "%s" % (
            self.name
        )
