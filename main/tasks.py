# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from celery import shared_task
from django.core.mail import send_mail


@shared_task
def send_contact_mail(data):
    """
        :param data:
            subject: string
            message: string
            recipient_list: string[]
        :type object:
    """
    send_mail(
        data.get('subject'),
        data.get('message'),
        None,
        data.get('recipient_list'),
        fail_silently=False
    )
