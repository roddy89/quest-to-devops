# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from main.models import Contact


@admin.register(Contact)
class CourseAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', )
    list_filter = ('name',)
